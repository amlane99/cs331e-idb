from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
#GCP port, change before final submission
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgresql://postgres:123@34.123.229.231:5432/postgres') #'sqlite:///goda.db'
#local port
#app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgresql://postgres:123@localhost:5432/postgres') #'sqlite:///goda.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)


class Planets(db.Model):
    __tablename__ = 'Planets'
    name = db.Column(db.String(80), nullable = False , primary_key = True)
    hostname = db.Column(db.String(80), nullable = False)
    number_of_stars = db.Column(db.Integer, nullable = True)
    discovery_year = db.Column(db.Integer, nullable = False)
    discovery_facility = db.Column(db.String(80), nullable = False)
    discovery_method =  db.Column(db.String(80), nullable = False)

class Orgs(db.Model):
    __tablename__ = 'DiscoveryFacilities'
    name = db.Column(db.String(80), nullable = True )
    locale = db.Column(db.String(80), nullable = True)
    telescope = db.Column(db.String(80), nullable = True)
    instruments = db.Column(db.String(80), nullable = True)
    id = db.Column(db.Integer, nullable = False , primary_key = True)

class Star(db.Model):
    __tablename__ = 'Star'
    hostname = db.Column(db.String(80), nullable = False, primary_key = True)
    number_of_planets = db.Column(db.Integer, nullable = True)
    spectral_type = db.Column(db.String(80), nullable = True)
    stellar_radius = db.Column(db.Numeric, nullable = True)
    stellar_temp = db.Column(db.Integer, nullable = True)
    stellar_mass = db.Column(db.Numeric, nullable = True)
    surface_gravity = db.Column(db.Numeric, nullable = True)
    discovery_facility = db.Column(db.String(80), nullable = True)

class GitStats(db.Model):
    __tablename__ = 'GitStats'
    gitID = db.Column(db.String(80), nullable = False, primary_key = True)
    commits = db.Column(db.Integer, nullable = False)
    issues = db.Column(db.Integer, nullable = False)
