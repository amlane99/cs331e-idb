from flask import Flask, render_template, request, url_for, redirect, json, jsonify, Response, make_response
from flask_sqlalchemy import SQLAlchemy
from create_db import create_planets, create_stars, create_orgs, create_gitstats
from models import app, db, Planets, Orgs, Star, GitStats
import requests
# db.drop_all()
# db.create_all()
# create_planets()
# create_stars()
# create_orgs()
# create_gitstats()
# planets = [{"name":"11 Com b", "hostname" : "11 Com", "number_of_stars": "2", "discovery_year": "2007", "discovery_facility":"Xinglong Station", "discovery_method":"Radial Velocity" },
#           {"name":"11 UMi b", "hostname" : "11 UMi", "number_of_stars": "1", "discovery_year": "2009", "discovery_facility":"Thueringer Landessternwarte Tautenburg", "discovery_method":"Radial Velocity"},
#           {"name":"14 And b", "hostname" : "14 And", "number_of_stars": "1", "discovery_year": "2008", "discovery_facility":"Okayama Astrophysical Observatory", "discovery_method":"Radial Velocity"}]

#stars = [{ "hostname" : "11 Com", "spectral_type" : "G8 III", "stellar_temp" : "4742", "stellar_radius" : "19", "stellar_mass" : "2.7", "surface_gravity" : "2.31", "number_of_planets" : "1", "discovery_facility":"Xinglong Station", "associated_planets": ["11 Com b"]},
#          { "hostname" : "11 UMi", "spectral_type" : "K4 III", "stellar_temp" : "4340", "stellar_radius" : "24.08", "stellar_mass" : "1.80", "surface_gravity" : "1.60", "number_of_planets" : "1", "discovery_facility":"Thueringer Landessternwarte Tautenburg", "associated_planets": ["11 UMi b"]},
#          { "hostname" : "14 And", "spectral_type" : "KO III", "stellar_temp" : "4813", "stellar_radius" : "11", "stellar_mass" : "2.2", "surface_gravity" : "2.63", "number_of_planets" : "1", "discovery_facility":"Okayama Astrophysical Observatory", "associated_planets": ["14 And b"]}]

# discovery_orgs = [{"discovery_facility":"Okayama Astrophysical Observatory", 'city':'Asakuchi', 'state':'Okayama', 'country':'Japan', 'planet':'14 And b',
#                  'hoststar':'14 And'},
#                   {"discovery_facility":"Thueringer Landessternwarte Tautenburg", 'city':'Tautenburg', 'state': 'Thuringia', 'country':'Germany',
#                  'planet':'11 UMi b', 'hoststar':'11 UMi'},
#                   {"discovery_facility":"Xinglong Station", 'city':'Cangzhou', 'state': 'Hebei', 'country':'China', 'planet':'11 Com b', 'hoststar':'11 Com'}]

@app.route('/')
def home():
    return render_template("project_home.html" )

@app.route('/search/', methods=['GET','POST'])
def search():
    if (request.method == 'POST'):
        display = True
        search = request.form.get("search")
        keyword = '%' + search + '%'
        planets = Planets.query.filter(Planets.name.like(keyword)).all()
        stars = Star.query.filter(Star.hostname.like(keyword)).all()
        orgs = Orgs.query.filter(Orgs.name.like(keyword)).all()

        filt = request.form.get('filterBy')
        if filt is not None:
            filterBy = str(filt)
            if filterBy == "planets":
                stars.clear()
                orgs.clear()
            elif filterBy == "stars":
                planets.clear()
                orgs.clear()
            elif filterBy == "orgs":
                planets.clear()
                stars.clear()

    else:
        display = False
        planets = []
        stars = []
        orgs = []

    return render_template("search.html", planets=planets,stars=stars,orgs=orgs,display=display)

PLANETS_PER_PAGE = 150
@app.route('/planet/')
def planet():
    page = request.args.get('page', 1, type=int)
    planets = db.session.query(Planets).paginate(page=page, per_page=PLANETS_PER_PAGE)
    return render_template("planet.html", planets = planets)

@app.route('/planet/json/')
def planet_json():
    planets = Planets.query.all()
    response = list()

    for p in planets:
        response.append({
            'name': p.name,
            'hostname': p.hostname,
            'number_of_stars': p.number_of_stars,
            'discovery_year': p.discovery_year,
            'discovery_facility': p.discovery_facility,
            'discovery_method': p.discovery_method
            })

    return make_response({
        'planets': response
        }, 200)

@app.route('/planet/<string:planets_name>/detail/')
def planet_detail(planets_name):
    planets = db.session.execute('select * from \"Planets\" where name =' + " '" + planets_name + "'").fetchall()

    return render_template("planet_detail.html", planet = planets[0], planets = planets)

@app.route('/planet/<string:planets_name>/detail/json/', methods=['GET','POST'])
def planet_detail_json(planets_name):
    planets = db.session.execute('select * from \"Planets\" where name =' + " '" + planets_name + "'").fetchall()

    response = dict()
    response['name'] = planets[0].name
    response['hostname'] = planets[0].hostname
    response['number_of_stars'] = planets[0].number_of_stars
    response['discovery_year'] = planets[0].discovery_year
    response['discovery_facility'] = planets[0].discovery_facility
    response['discovery_method'] = planets[0].discovery_method

    return make_response(response, 200)

STARS_PER_PAGE = 150
@app.route('/star/')
def star():
    page = request.args.get('page', 1, type=int)
    stars = db.session.query(Star).paginate(page=page, per_page=STARS_PER_PAGE)
    return render_template("star.html", stars = stars)

@app.route('/star/json')
def star_json():
    stars = Star.query.all()
    response = list()

    for s in stars:
        response.append({
            'hostname': s.hostname,
            'number_of_planets': s.number_of_planets,
            'spectral_type': s.spectral_type,
            'stellar_radius': str(s.stellar_radius),
            'stellar_temp': s.stellar_temp,
            'stellar_mass': str(s.stellar_mass),
            'surface_gravity': str(s.surface_gravity),
            'discovery_facility': s.discovery_facility
            })

    return make_response({
        'stars': response
        }, 200)

@app.route('/star/<string:star_name>/detail/')
def star_detail(star_name):
    stars = db.session.execute('select * from \"Planets\" \"a\" left join \"Star\" \"b\" on \"a\".\"hostname\" = \"b\".\"hostname\" where \"b\".\"hostname\" = ' + " '" + star_name + "'").fetchall()
    star = dict()
    star["associated_planets"] = []
    for i in stars:
        star["hostname"] = i.hostname
        star["spectral_type"] = i.spectral_type
        star["stellar_temp"] = i.stellar_temp
        star["stellar_radius"] = i.stellar_radius
        star["stellar_mass"] = i.stellar_mass
        star["surface_gravity"] = i.surface_gravity
        star["number_of_planets"] = i.number_of_planets
        star["discovery_facility"] = i.discovery_facility
        star["associated_planets"].append(i.name)
    return render_template("star_detail.html", star = star, stars = stars)

@app.route('/star/<string:star_name>/detail/json', methods=['GET','POST'])
def star_detail_json(star_name):
    stars = db.session.execute('select * from \"Planets\" \"a\" left join \"Star\" \"b\" on \"a\".\"hostname\" = \"b\".\"hostname\" where \"b\".\"hostname\" = ' + " '" + star_name + "'").fetchall()
    star = dict()
    star["associated_planets"] = []

    for i in stars:
        star["hostname"] = i.hostname
        star["spectral_type"] = i.spectral_type
        star["stellar_temp"] = i.stellar_temp
        star["stellar_radius"] = str(i.stellar_radius)
        star["stellar_mass"] = str(i.stellar_mass)
        star["surface_gravity"] = str(i.surface_gravity)
        star["number_of_planets"] = i.number_of_planets
        star["discovery_facility"] = i.discovery_facility
        star["associated_planets"].append(i.name)

    return make_response(star, 200)

ORGS_PER_PAGE = 24
@app.route('/discovery/')
def discovery_org():
    page = request.args.get('page', 1, type=int)
    orgs = db.session.query(Orgs).paginate(page=page, per_page=ORGS_PER_PAGE)
    return render_template("discovery_org.html", orgs = orgs)

@app.route('/discovery/json')
def discovery_org_json():
    orgs = Orgs.query.all()
    response = list()

    for o in orgs:
        response.append({
            'id': o.id,
            'name': o.name,
            'locale': o.locale,
            'telescope': o.telescope,
            'instruments': o.instruments
            })

    return make_response({
        'discovery_orgs': response
        }, 200)

@app.route('/discovery/<string:org_name>/detail/')
def org_detail(org_name):
    orgs = db.session.execute('select \"b\".name, \"b\".hostname, \"b\".discovery_facility, \"a\".locale,\"a\".telescope, \"a\".instruments from \"Planets\" \"b\" left join \"DiscoveryFacilities\" \"a\" on \"a\".name = \"b\".discovery_facility where \"b\".discovery_facility =' + " '" + org_name + "'").fetchall()
    org = dict()
    org["telescope"] = []
    org["instruments"] = []
    org["name"] = []
    org["hostname"] = []
    org["locale"] = []
    for i in orgs:
        org["discovery_facility"] = i.discovery_facility
        if i.telescope not in org["telescope"]:
            org["telescope"].append(i.telescope)
        if i.instruments not in org["instruments"]:
            org["instruments"].append(i.instruments)
        if i.hostname not in org["hostname"]:
            org["hostname"].append(i.hostname)
        if i.name not in org["name"]:
            org["name"].append(i.name)
        if i.locale not in org["locale"]:
            org["locale"].append(i.locale)

    return render_template("org_detail.html", org = org, orgs = orgs)

@app.route('/discovery/<string:org_name>/detail/json')
def org_detail_json(org_name):
    orgs = db.session.execute('select \"b\".name, \"b\".hostname, \"b\".discovery_facility, \"a\".locale,\"a\".telescope, \"a\".instruments from \"Planets\" \"b\" left join \"DiscoveryFacilities\" \"a\" on \"a\".name = \"b\".discovery_facility where \"b\".discovery_facility =' + " '" + org_name + "'").fetchall()
    org = dict()
    org["telescope"] = []
    org["instruments"] = []
    org["associated_planets"] = []
    org["associated_stars"] = []
    org["locale"] = []
    for i in orgs:
        org["discovery_facility"] = i.discovery_facility
        if i.telescope not in org["telescope"]:
            org["telescope"].append(i.telescope)
        if i.instruments not in org["instruments"]:
            org["instruments"].append(i.instruments)
        if i.hostname not in org["associated_stars"]:
            org["associated_stars"].append(i.hostname)
        if i.name not in org["associated_planets"]:
            org["associated_planets"].append(i.name)
        if i.locale not in org["locale"]:
            org["locale"].append(i.locale)

    return make_response(org, 200)

@app.route('/about/')
def about():
    gitstats = db.session.query(GitStats).all()
    return render_template("about.html", gitstats = gitstats)

@app.route('/about/json/')
def about_json():
    gitstats = GitStats.query.all()
    response = list()

    for g in gitstats:
            response.append({
                'gitID': g.gitID,
                'commits': g.commits,
                'issues': g.issues
            })

    return make_response({
        'gitstats': response
        }, 200)


#
# if __name__ == '__main__':
#    app.debug = True
#    app.run()

if __name__ == '__main__':
	#app.debug = True
	app.run()
