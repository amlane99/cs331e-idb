import os
import sys
import unittest
from models import db, Planets, Orgs, Star

# -----------
# DBTestCases
# -----------
class DBTestCases(unittest.TestCase):
    # ---------
    # insertion
    # ---------


    def test_source_insert_1(self):
        s = Planets(name='Yang 21', hostname = 'Yang', number_of_stars = 1, discovery_year = 2000, discovery_facility = 'Ceaspe', discovery_method = 'Raser')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Planets).filter_by(name = 'Yang 21').one()
        self.assertEqual(str(r.discovery_method), 'Raser')

        db.session.query(Planets).filter_by(name = 'Yang 21').delete()
        db.session.commit()

    def test_source_insert_2(self):
        s = Star(hostname = "Yang", number_of_planets = 2, spectral_type = "Human", stellar_radius = 60, stellar_temp = 35,
                 stellar_mass = 45, surface_gravity = 9.8, discovery_facility = 'Ceaspe')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Star).filter_by(hostname = 'Yang').one()
        self.assertEqual(int(r.stellar_radius), 60)

        db.session.query(Star).filter_by(hostname = 'Yang').delete()
        db.session.commit()

    def test_source_insert_3(self):
        s = Orgs(name = "Ceaspe", locale = "Ground", telescope = "Dospt", instruments = "Wensdy 12", id = 99999)
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Orgs).filter_by(id = 99999).one()
        self.assertEqual(str(r.locale), "Ground")

        db.session.query(Orgs).filter_by(id = 99999).delete()
        db.session.commit()

    def test_source_insert_4(self):
        s = Planets(name='', hostname = '', number_of_stars = 0, discovery_year = 0, discovery_facility = '', discovery_method = '')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Planets).filter_by(name = '').one()
        self.assertEqual(str(r.discovery_facility), '')

        db.session.query(Planets).filter_by(name = '').delete()
        db.session.commit()

    def test_source_insert_5(self):
        s = Star(hostname = "~~", number_of_planets = 2, spectral_type = "rainbow", stellar_radius = 60, stellar_temp = 35,
                 stellar_mass = 45, surface_gravity = 9.8, discovery_facility = 4)
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Star).filter_by(hostname = '~~').one()
        self.assertEqual(int(r.discovery_facility), 4)

        db.session.query(Star).filter_by(hostname = '~~').delete()
        db.session.commit()

    def test_source_insert_6(self):
        s = Orgs(name = 100, locale = 10, telescope = "i don't know", instruments = 75, id = 88888)
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Orgs).filter_by(id = 88888).one()
        self.assertEqual(int(r.instruments), 75)

        db.session.query(Orgs).filter_by(id = 88888).delete()
        db.session.commit()

    def test_source_insert_7(self):
        s = Planets(name='star5000', hostname = 'tatooine', number_of_stars = 2, discovery_year = 3045, discovery_facility = 'Skywalker', discovery_method = 'Eyes')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Planets).filter_by(hostname = 'tatooine').one()
        self.assertEqual(str(r.discovery_method), 'Eyes')

        db.session.query(Planets).filter_by(name = 'star5000').delete()
        db.session.commit()

    def test_source_insert_8(self):
        #s = Star(hostname = 312769, number_of_planets = 3, spectral_type = "blue", stellar_radius = 'millions', stellar_temp = 'billion',
         #        stellar_mass = 0 , surface_gravity = 70, discovery_facility = 'Harneet')
        s = Star(hostname = '312769', number_of_planets = 3, spectral_type = "blue", stellar_radius = 10000.38, stellar_temp = 5,
               stellar_mass = 4/5 , surface_gravity = 0, discovery_facility = 'Harneet')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Star).filter_by(hostname = '312769').one()
        self.assertEqual(float(r.stellar_mass),(4/5) )

        db.session.query(Star).filter_by(hostname = '312769').delete()
        db.session.commit()

    def test_source_insert_9(self):
        s = Orgs(name = "Harneet", locale = "Earth", telescope = "phone", instruments = "home", id = 000)
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Orgs).filter_by(id = 0).one()
        self.assertEqual(str(r.locale), "Earth")

        db.session.query(Orgs).filter_by(id = 0).delete()
        db.session.commit()

    def test_source_insert_10(self):
        s = Planets(name='HWIO 2', hostname = 'HWIO', number_of_stars = 2, discovery_year = 1978, discovery_facility = 'NASA', discovery_method = 'Some method')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Planets).filter_by(name = 'HWIO 2').one()
        self.assertEqual(str(r.discovery_facility), 'NASA')

        db.session.query(Planets).filter_by(name = 'HWIO 2').delete()
        db.session.commit()

    def test_source_insert_11(self):
        s = Star(hostname = "HWIO", number_of_planets = 2, spectral_type = "IUWB", stellar_radius = 3.5, stellar_temp = 1200,
                 stellar_mass = 899.15, surface_gravity = 30.2, discovery_facility = 'NASA')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Star).filter_by(hostname = 'HWIO').one()
        self.assertEqual(float(r.stellar_mass), 899.15)

        db.session.query(Star).filter_by(hostname = 'HWIO').delete()
        db.session.commit()

    def test_source_insert_12(self):
        s = Orgs(name = "NASA", locale = "Space", telescope = "Hdsjad 78", instruments = "Iospast 91", id = 10000)
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Orgs).filter_by(id = 10000).one()
        self.assertEqual(str(r.locale), "Space")

        db.session.query(Orgs).filter_by(id = 10000).delete()
        db.session.commit()

    def test_source_insert_13(self):
        s = Planets(name='PPWE 90', hostname = 'PPWE', number_of_stars = 7, discovery_year = 1867, discovery_facility = 'Xiaolong station', discovery_method = 'Raser')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Planets).filter_by(name = 'PPWE 90').one()
        self.assertEqual(str(r.discovery_method), 'Raser')

        db.session.query(Planets).filter_by(name = 'PPWE 90').delete()
        db.session.commit()

    def test_source_insert_14(self):
        s = Star(hostname = "PPWE", number_of_planets = 26, spectral_type = "WQPO", stellar_radius = 24.7, stellar_temp = 340,
                 stellar_mass = 10.89, surface_gravity = 2.3, discovery_facility = 'Xiaolong station')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Star).filter_by(hostname = 'PPWE').one()
        self.assertEqual(float(r.stellar_radius), 24.7)

        db.session.query(Star).filter_by(hostname = 'PPWE').delete()
        db.session.commit()

    def test_source_insert_15(self):
        s = Orgs(name = "Xiaolong station", locale = "Ground", telescope = "Wesde", instruments = "Pogqel", id = 4567)
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Orgs).filter_by(id = 4567).one()
        self.assertEqual(str(r.telescope), "Wesde")

        db.session.query(Orgs).filter_by(id = 4567).delete()
        db.session.commit()

    def test_source_insert_16(self):
        s = Planets(name='wosjdnso', hostname = 'WTO', number_of_stars = 1, discovery_year = 2009, discovery_facility = 'Natioinal station', discovery_method = 'Some method')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Planets).filter_by(name = 'wosjdnso').one()
        self.assertEqual(int(r.discovery_year), 2009)

        db.session.query(Planets).filter_by(name = 'wosjdnso').delete()
        db.session.commit()

    def test_source_insert_17(self):
        s = Star(hostname = "WTO", number_of_planets = 1, spectral_type = "PTERRY", stellar_radius = 7.7, stellar_temp = 400,
                 stellar_mass = 205.9, surface_gravity = 10.5, discovery_facility = 'Natioinal station')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Star).filter_by(hostname = 'WTO').one()
        self.assertEqual(float(r.surface_gravity), 10.5)

        db.session.query(Star).filter_by(hostname = 'WTO').delete()
        db.session.commit()

    def test_source_insert_18(self):
        s = Orgs(name = 'Natioinal station', locale = "Space", telescope = "Bestoe", instruments = "Prestoa", id = 8923)
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Orgs).filter_by(id = 8923).one()
        self.assertEqual(str(r.telescope), "Bestoe")

        db.session.query(Orgs).filter_by(id = 8923).delete()
        db.session.commit()

    def test_source_insert_19(self):
        s = Planets(name='PWCTE', hostname = 'PW', number_of_stars = 6, discovery_year = 2021, discovery_facility = 'NASA', discovery_method = 'telescope')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Planets).filter_by(name = 'PWCTE').one()
        self.assertEqual(int(r.discovery_year), 2021)

        db.session.query(Planets).filter_by(name = 'PWCTE').delete()
        db.session.commit()

    def test_source_insert_20(self):
        s = Star(hostname = "PW", number_of_planets = 15, spectral_type = "PPPW", stellar_radius = 20.5, stellar_temp = 80000,
                 stellar_mass = 10.264, surface_gravity = 41.78, discovery_facility = 'NASA')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Star).filter_by(hostname = 'PW').one()
        self.assertEqual(float(r.surface_gravity), 41.78)

        db.session.query(Star).filter_by(hostname = 'PW').delete()
        db.session.commit()

    def test_source_insert_21(self):
        s = Orgs(name = "NASA", locale = "Ground", telescope = "TK178", instruments = "OP515", id = 22222)
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Orgs).filter_by(id = 22222).one()
        self.assertEqual(str(r.telescope), "TK178")

        db.session.query(Orgs).filter_by(id = 22222).delete()
        db.session.commit()

if __name__ == '__main__':
    unittest.main()
# end of code
