from models import app, db, Planets, Star, Orgs, GitStats
import requests

def create_planets():
    """
    populate planet table
    """
    response = requests.get("https://exoplanetarchive.ipac.caltech.edu/cgi-bin/nstedAPI/nph-nstedAPI?table=exoplanets&select=pl_hostname,pl_pnum,st_spstr,st_rad,st_teff,st_mass,st_logg,pl_name,pl_facility,pl_discmethod,pl_snum,pl_disc&order=pl_hostname&format=json")
    json_data = response.json()
    lst = []

    for onePlanet in json_data:
        name = onePlanet['pl_name']
        hostname = onePlanet['pl_hostname']
        number_of_stars = onePlanet['pl_snum']
        discovery_year = onePlanet['pl_disc']
        discovery_facility = onePlanet['pl_facility']
        discovery_method =  onePlanet['pl_discmethod']
        if name in lst:
            continue
        else:
            lst.append(name)
            newPlanet = Planets(name = name, hostname = hostname, number_of_stars = number_of_stars, discovery_year = discovery_year, discovery_facility = discovery_facility, discovery_method = discovery_method)

        # After I create the Plant, I can then add it to my session.
        db.session.add(newPlanet)
        # commit the session to my DB.
        db.session.commit()

def create_stars():
    """
    populate star table
    """
    response = requests.get("https://exoplanetarchive.ipac.caltech.edu/cgi-bin/nstedAPI/nph-nstedAPI?table=exoplanets&select=pl_hostname,pl_pnum,st_spstr,st_rad,st_teff,st_mass,st_logg,pl_name,pl_facility,pl_discmethod,pl_disc&order=pl_hostname&format=json")
    star = response.json()
    lst = []

    for oneStar in star:
        hostname = oneStar['pl_hostname']
        number_of_planets = oneStar['pl_pnum']
        spectral_type = oneStar['st_spstr']
        stellar_radius = oneStar['st_rad']
        stellar_temp = oneStar['st_teff']
        stellar_mass = oneStar['st_mass']
        surface_gravity = oneStar['st_logg']
        discovery_facility = oneStar['pl_facility']

        if hostname in lst:
            continue
        else:
            lst.append(hostname)
            newStar = Star(hostname = hostname, number_of_planets = number_of_planets, spectral_type = spectral_type, stellar_radius = stellar_radius, stellar_temp = stellar_temp, stellar_mass = stellar_mass, surface_gravity = surface_gravity, discovery_facility = discovery_facility)

        # After I create the star, I can then add it to my session.
        db.session.add(newStar)
        # commit the session to my DB.
        db.session.commit()

def create_orgs():
    """
    populate orgs table
    """
    response = requests.get("https://exoplanetarchive.ipac.caltech.edu/cgi-bin/nstedAPI/nph-nstedAPI?table=exoplanets&select=pl_locale,pl_telescope,pl_facility,pl_instrument&format=json")
    json_data = response.json()
    lst = []
    counter = 0

    for oneOrg in json_data:
        if oneOrg in lst:
            continue
        else:
            counter += 1
            lst.append(oneOrg)
            name = oneOrg['pl_facility']
            locale = oneOrg['pl_locale']
            telescope = oneOrg['pl_telescope']
            instruments = oneOrg['pl_instrument']
            newOrg = Orgs(name = name, locale = locale, telescope = telescope, instruments = instruments, id = counter)

        # After I create the Plant, I can then add it to my session.
        db.session.add(newOrg)
        # commit the session to my DB.
        db.session.commit()

def create_gitstats():
    authors = ["Allison Lane","Yang Wang","Aaron Furman","Haley Roe","Mark Cahill","Harneet Kaur"]
    gitIDs = ["amlane99","yw22729","awf9983","haleyroe","markbaconcahill","hkaur00"]
    commits_response = requests.get("https://gitlab.com/api/v4/projects/24371898/repository/commits?per_page=100&format=json&ref_name=master")
    issues_response = requests.get("https://gitlab.com/api/v4/projects/24371898/issues?per_page=100&format=json")
    commits = commits_response.json()
    issues = issues_response.json()
    for i in range(6):
        commit_count = 0
        issue_count = 0
        for commit in commits:
            if (commit['committer_name'] == authors[int(i)]) or (commit['committer_name'] == gitIDs[int(i)]):
                commit_count += 1
        for issue in issues:
            if issue['author']['username'] == gitIDs[int(i)]:
                issue_count += 1
        newGitStats = GitStats(gitID = gitIDs[i], commits = commit_count, issues = issue_count)
        db.session.add(newGitStats)
        db.session.commit()
